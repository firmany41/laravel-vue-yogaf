-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.33 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for apotek
CREATE DATABASE IF NOT EXISTS `apotek` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `apotek`;

-- Dumping structure for table apotek.detail_transaksi
CREATE TABLE IF NOT EXISTS `detail_transaksi` (
  `id_detail_transaksi` int(11) NOT NULL AUTO_INCREMENT,
  `id_transaksi` int(11) DEFAULT NULL,
  `id_obat` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_detail_transaksi`),
  KEY `FK__transaksi` (`id_transaksi`),
  KEY `FK__obat` (`id_obat`),
  CONSTRAINT `FK__obat` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK__transaksi` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id_transaksi`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table apotek.detail_transaksi: ~0 rows (approximately)
/*!40000 ALTER TABLE `detail_transaksi` DISABLE KEYS */;
INSERT IGNORE INTO `detail_transaksi` (`id_detail_transaksi`, `id_transaksi`, `id_obat`) VALUES
	(1, 1, 1),
	(2, 1, 2),
	(3, 1, 3),
	(4, 2, 3),
	(5, 2, 4),
	(6, 3, 4),
	(7, 4, 4),
	(8, 4, 7),
	(9, 5, 6),
	(10, 5, 8),
	(11, 6, 7),
	(12, 6, 8),
	(13, 7, 5),
	(14, 7, 6),
	(15, 8, 6),
	(16, 9, 6),
	(17, 9, 8),
	(18, 9, 9),
	(19, 10, 10);
/*!40000 ALTER TABLE `detail_transaksi` ENABLE KEYS */;

-- Dumping structure for table apotek.member
CREATE TABLE IF NOT EXISTS `member` (
  `id_member` int(11) NOT NULL AUTO_INCREMENT,
  `id_pembeli` int(11) NOT NULL,
  `tgl_daftar` date NOT NULL,
  `keterangan` varchar(250) NOT NULL,
  PRIMARY KEY (`id_member`),
  KEY `id_pembeli1` (`id_pembeli`),
  CONSTRAINT `id_pembeli1` FOREIGN KEY (`id_pembeli`) REFERENCES `pembeli` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table apotek.member: ~0 rows (approximately)
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT IGNORE INTO `member` (`id_member`, `id_pembeli`, `tgl_daftar`, `keterangan`) VALUES
	(1, 1, '2004-02-24', 'A'),
	(2, 2, '2003-02-24', 'B'),
	(3, 3, '2002-02-24', 'C'),
	(4, 4, '2001-02-24', 'D'),
	(5, 5, '2000-02-24', 'E'),
	(6, 6, '2005-02-24', 'F'),
	(7, 7, '2006-02-24', 'G'),
	(8, 8, '2007-02-24', 'H'),
	(9, 9, '2008-02-24', 'I'),
	(10, 10, '2009-02-24', 'J');
/*!40000 ALTER TABLE `member` ENABLE KEYS */;

-- Dumping structure for table apotek.obat
CREATE TABLE IF NOT EXISTS `obat` (
  `id_obat` int(11) NOT NULL AUTO_INCREMENT,
  `nama_obat` varchar(50) NOT NULL,
  `jenis_obat` varchar(50) NOT NULL,
  `komposisi` varchar(50) DEFAULT NULL,
  `khasiat` varchar(50) DEFAULT NULL,
  `efek_samping` varchar(50) DEFAULT NULL,
  `tgl_kadaluwarsa` date NOT NULL,
  `stok` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  PRIMARY KEY (`id_obat`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table apotek.obat: ~0 rows (approximately)
/*!40000 ALTER TABLE `obat` DISABLE KEYS */;
INSERT IGNORE INTO `obat` (`id_obat`, `nama_obat`, `jenis_obat`, `komposisi`, `khasiat`, `efek_samping`, `tgl_kadaluwarsa`, `stok`, `harga`) VALUES
	(1, 'Obat A', 'A', 'A', NULL, NULL, '2024-02-24', 10, 1000),
	(2, 'Obat B', 'B', 'B', NULL, NULL, '2024-02-24', 11, 1000),
	(3, 'Obat C', 'C', 'C', NULL, NULL, '2025-02-24', 12, 1000),
	(4, 'Obat D', 'D', 'D', NULL, NULL, '2026-02-24', 13, 2000),
	(5, 'Obat E', 'E', 'E', NULL, NULL, '2027-02-24', 14, 2000),
	(6, 'Obat F', 'F', 'F', NULL, NULL, '2028-02-24', 15, 2000),
	(7, 'Obat G', 'G', 'G', NULL, NULL, '2028-03-24', 16, 3000),
	(8, 'Obat H', 'H', 'H', NULL, NULL, '2028-04-24', 18, 3000),
	(9, 'Obat I', 'I', 'I', NULL, NULL, '2028-05-24', 19, 3000),
	(10, 'Obat J', 'J', 'J', NULL, NULL, '2028-07-24', 19, 10000);
/*!40000 ALTER TABLE `obat` ENABLE KEYS */;

-- Dumping structure for table apotek.pembeli
CREATE TABLE IF NOT EXISTS `pembeli` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `no_hp` char(13) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table apotek.pembeli: ~0 rows (approximately)
/*!40000 ALTER TABLE `pembeli` DISABLE KEYS */;
INSERT IGNORE INTO `pembeli` (`id`, `nama`, `tgl_lahir`, `alamat`, `no_hp`) VALUES
	(1, 'Pembeli A', '2004-02-24', 'Alamat A', '082222333444'),
	(2, 'Pembeli B', '2003-02-24', 'Alamat B', '081222333444'),
	(3, 'Pembeli C', '2002-02-24', 'Alamat C', '081222333444'),
	(4, 'Pembeli D', '2001-02-24', 'Alamat D', '081222333444'),
	(5, 'Pembeli E', '2000-02-24', 'Alamat E', '081222333444'),
	(6, 'Pembeli F', '2005-02-24', 'Alamat F', '081222333424'),
	(7, 'Pembeli G', '2006-02-24', 'Alamat G', '081222333442'),
	(8, 'Pembeli H', '2007-02-24', 'Alamat H', '081222332444'),
	(9, 'Pembeli I', '2008-02-24', 'Alamat I', '081222323444'),
	(10, 'Pembeli J', '2009-02-24', 'Alamat J', '081221333444');
/*!40000 ALTER TABLE `pembeli` ENABLE KEYS */;

-- Dumping structure for table apotek.transaksi
CREATE TABLE IF NOT EXISTS `transaksi` (
  `id_transaksi` int(11) NOT NULL AUTO_INCREMENT,
  `id_pembeli` int(11) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `total_harga` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_transaksi`),
  KEY `FK__pembeli` (`id_pembeli`),
  CONSTRAINT `FK__pembeli` FOREIGN KEY (`id_pembeli`) REFERENCES `pembeli` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table apotek.transaksi: ~0 rows (approximately)
/*!40000 ALTER TABLE `transaksi` DISABLE KEYS */;
INSERT IGNORE INTO `transaksi` (`id_transaksi`, `id_pembeli`, `tgl_transaksi`, `total_harga`) VALUES
	(1, 1, '2024-03-24', 3000),
	(2, 2, '2024-04-24', 3000),
	(3, 3, '2024-04-24', 2000),
	(4, 1, '2024-04-24', 5000),
	(5, 2, '2024-05-24', 5000),
	(6, 3, '2024-05-24', 6000),
	(7, 5, '2024-05-24', 4000),
	(8, 6, '2024-05-24', 2000),
	(9, 8, '2024-06-24', 8000),
	(10, 10, '2024-06-24', 10000);
/*!40000 ALTER TABLE `transaksi` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
