<?php
$n = 10;
echo "<table border =\"1\" style='border-collapse: collapse'>";
echo "<tr style='background-color: #728FCE;'><th>No</th><th>Nama</th><th>Kelas</th></tr>";
for ($i = 0; $i < $n; $i++) {
    $no = $i + 1;
    $kelas = $n - $i;
    $mod = $i % 2;
    if ($mod == 0) {
        echo "<tr style='background-color: #B6B6B4;'>";
    } else {
        echo "<tr>";
    }
    
    echo "<td>$no</td><td>Nama ke $no</td><td>Kelas $kelas</td>";
    echo "</tr>";
}
echo "</table>";
