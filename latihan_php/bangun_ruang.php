<?php
$a = 8;
$b = 10;
$c = 15;

//rumus volume kubur
$d = $a * $a * $a;
echo "1. Volume kubus dengan sisi $a cm adalah $a x $a x $a = $d cm<sup>3</sup><br>";
echo "<hr>";

//rumus volume balok
$d = $c * $b * $a;
echo "2. Volume balok dengan panjang $c cm, lebar $b cm dan tinggi $c cm adalah $c x $b x $a = $d cm<sup>3</sup><br>";
echo "<hr>";

//rumus volume silinder
$d = ((22/7) * $a * $a) * $b;
echo "3. Volume silinde dengan jari-jari lingkaran $a cm dan tinggi $b cm adalah π x $a x $a x $b = $d cm<sup>3</sup><br>";
echo "<hr>";
