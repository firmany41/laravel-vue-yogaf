<?php
include_once("connectdb.php");
$buku = mysqli_query($mysqli, "SELECT buku.*, nama_pengarang, nama_penerbit, katalog.nama as nama_katalog FROM buku 
                                        LEFT JOIN  pengarang ON pengarang.id_pengarang = buku.id_pengarang
                                        LEFT JOIN  penerbit ON penerbit.id_penerbit = buku.id_penerbit
                                        LEFT JOIN  katalog ON katalog.id_katalog = buku.id_katalog
                                        ORDER BY judul ASC");

$penerbit = mysqli_query($mysqli, "SELECT * FROM penerbit ORDER BY id_penerbit ASC");
$pengarang = mysqli_query($mysqli, "SELECT * FROM pengarang ORDER BY id_pengarang ASC");
$katalog = mysqli_query($mysqli, "SELECT * FROM katalog ORDER BY id_katalog ASC");
?>

<html>

<head>
    <title>Homepage</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</head>

<body class="bg-light">
    <nav class="navbar navbar-expand-lg navbar-light bg-white">
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 m-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#buku" class="text-decoration-none">Buku</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#penerbit" class="text-decoration-none">Penerbit</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#pengarang" class="text-decoration-none">Pengarang</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#katalog" class="text-decoration-none">Katalog</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container mx-auto my-2 bg-white">
        <section id="buku">
            <a class="btn btn-primary mt-2" role="button" href="addBuku.php">Add New Buku</a><br /><br />

            <table class="table table-striped table-bordered">

                <tr>
                    <th>ISBN</th>
                    <th>Judul</th>
                    <th>Tahun</th>
                    <th>Pengarang</th>
                    <th>Penerbit</th>
                    <th>Katalog</th>
                    <th>Stok</th>
                    <th>Harga Pinjam</th>
                    <th>Aksi</th>
                </tr>
                <?php
                while ($buku_data = mysqli_fetch_array($buku)) {
                    echo "<tr>";
                    echo "<td>" . $buku_data['isbn'] . "</td>";
                    echo "<td>" . $buku_data['judul'] . "</td>";
                    echo "<td>" . $buku_data['tahun'] . "</td>";
                    echo "<td>" . $buku_data['nama_pengarang'] . "</td>";
                    echo "<td>" . $buku_data['nama_penerbit'] . "</td>";
                    echo "<td>" . $buku_data['nama_katalog'] . "</td>";
                    echo "<td>" . $buku_data['qty_stok'] . "</td>";
                    echo "<td>" . $buku_data['harga_pinjam'] . "</td>";
                    echo "<td><a class='btn btn-primary' href='editBuku.php?isbn=$buku_data[isbn]'>Edit</a> | <a class='btn btn-danger' href='deleteBuku.php?isbn=$buku_data[isbn]'>Delete</a></td></tr>";
                }
                ?>
            </table>
            <hr>
        </section>

        <section id="penerbit">
            <a class="btn btn-primary mt-2" role="button" href="addPenerbit.php">Add New Penerbit</a><br /><br />

            <table class="table table-striped table-bordered">

                <tr>
                    <th>Id</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Telp</th>
                    <th>Alamat</th>
                    <th>Aksi</th>
                </tr>
                <?php
                while ($penerbit_data = mysqli_fetch_array($penerbit)) {
                    echo "<tr>";
                    echo "<td>" . $penerbit_data['id_penerbit'] . "</td>";
                    echo "<td>" . $penerbit_data['nama_penerbit'] . "</td>";
                    echo "<td>" . $penerbit_data['email'] . "</td>";
                    echo "<td>" . $penerbit_data['telp'] . "</td>";
                    echo "<td>" . $penerbit_data['alamat'] . "</td>";
                    echo "<td><a class='btn btn-primary' href='editPenerbit.php?id_penerbit=$penerbit_data[id_penerbit]'>Edit</a> | <a class='btn btn-danger' href='deletePenerbit.php?id_penerbit=$penerbit_data[id_penerbit]'>Delete</a></td></tr>";
                }
                ?>
            </table>
            <hr>
        </section>

        <section id="pengarang">
            <a class="btn btn-primary mt-2" role="button" href="addPengarang.php">Add New Pengarang</a><br /><br />

            <table class="table table-striped table-bordered">

                <tr>
                    <th>Id</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Telp</th>
                    <th>Alamat</th>
                    <th>Aksi</th>
                </tr>
                <?php
                while ($pengarang_data = mysqli_fetch_array($pengarang)) {
                    echo "<tr>";
                    echo "<td>" . $pengarang_data['id_pengarang'] . "</td>";
                    echo "<td>" . $pengarang_data['nama_pengarang'] . "</td>";
                    echo "<td>" . $pengarang_data['email'] . "</td>";
                    echo "<td>" . $pengarang_data['telp'] . "</td>";
                    echo "<td>" . $pengarang_data['alamat'] . "</td>";
                    echo "<td><a class='btn btn-primary' href='editPengarang.php?id_pengarang=$pengarang_data[id_pengarang]'>Edit</a> | <a class='btn btn-danger' href='deletePengarang.php?id_pengarang=$pengarang_data[id_pengarang]'>Delete</a></td></tr>";
                }
                ?>
            </table>
            <hr>
        </section>

        <section id="katalog">
            <a class="btn btn-primary mt-2" role="button" href="addKatalog.php">Add New Katalog</a><br /><br />

            <table class="table table-striped table-bordered">

                <tr>
                    <th>Id</th>
                    <th>Nama</th>
                    <th>Aksi</th>
                </tr>
                <?php
                while ($katalog_data = mysqli_fetch_array($katalog)) {
                    echo "<tr>";
                    echo "<td>" . $katalog_data['id_katalog'] . "</td>";
                    echo "<td>" . $katalog_data['nama'] . "</td>";
                    echo "<td><a class='btn btn-primary' href='editKatalog.php?id_katalog=$katalog_data[id_katalog]'>Edit</a> | <a class='btn btn-danger' href='deleteKatalog.php?id_katalog=$katalog_data[id_katalog]'>Delete</a></td></tr>";
                }
                ?>
            </table>
            <hr>
        </section>
    </div>
</body>

</html>