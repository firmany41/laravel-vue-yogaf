<?php
function luaspersegi($sisi)
{
    $luaspersegi = $sisi * $sisi;
    echo "Hasil hitung luas persegi adalah sebagai berikut:<br />";
    echo "Diketahui;<br />";
    echo "Sisi = $sisi<br />";
    echo "Maka luas persegi sama dengan [ $sisi x $sisi ] = $luaspersegi";
    echo "<hr><br>";
};

function luaspersegipanjang($p, $l)
{
    $luaspersegipanjang = $p * $l;
    echo "Hasil hitung luas persegi panjang adalah sebagai berikut:<br />";
    echo "Diketahui;<br />";
    echo "Panjang = $p<br />";
    echo "Lebar = $l<br />";
    echo "Maka luas persegi panjang sama dengan [ $p x $l ] = $luaspersegipanjang";
    echo "<hr><br>";
};

function luassegitiga($a, $t)
{
    $luassegitiga = 1/2 * $a * $t;
    echo "Hasil hitung luas segitiga adalah sebagai berikut:<br />";
    echo "Diketahui;<br />";
    echo "Alas = $a<br />";
    echo "Tinggi = $t<br />";
    echo "Maka luas segitiga sama dengan [ 1/2 x $a x $t ] = $luassegitiga";
    echo "<hr><br>";
};

function volumekubus($sisi)
{
    $volumekubus = $sisi * $sisi * $sisi;
    echo "Hasil hitung volume kubus adalah sebagai berikut:<br />";
    echo "Diketahui;<br />";
    echo "Sisi = $sisi<br />";
    echo "Maka volume balok sama dengan [ $sisi x $sisi x $sisi ] = $volumekubus";
    echo "<hr><br>";
};

function volumebalok($p, $l, $t)
{
    $volumebalok = $p * $l * $t;
    echo "Hasil hitung volume balok adalah sebagai berikut:<br />";
    echo "Diketahui;<br />";
    echo "Panjang = $p<br />";
    echo "Lebar = $l<br />";
    echo "Tinggi = $t<br />";
    echo "Maka volume balok sama dengan [ $p x $l x $t ] = $volumebalok";
    echo "<hr><br>";
};


luaspersegi(5);
luaspersegipanjang(10, 5);
luassegitiga(5, 8);
volumekubus(5);
volumebalok(10, 5, 5);

?>