<?php
$db_host = "localhost";
$db_user = "root";
$db_pass = "";
$db_name = "perpus";

$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

if (!$conn) {
    die('Gagal melakukan koneksi ke Database : ' . mysqli_connect_error());
}

echo "<h2>Data Buku</h2><hr>";
$sql = "SELECT * FROM buku";
$result = $conn->query($sql);

if($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        echo "Buku : " . $row["isbn"] . " " . $row["judul"] . "<br>";
    } 
} else {
    echo "0 result";
}
echo "<hr>";

echo "<h2>Data Peminjaman</h2><hr>";
$sql2 = "SELECT anggota.nama, peminjaman.tgl_pinjam, peminjaman.tgl_kembali FROM anggota JOIN peminjaman ON anggota.id_anggota = peminjaman.id_anggota";
$result = $conn->query($sql2);

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $tgl_pinjam = new DateTime($row["tgl_pinjam"]);
        $tgl_kembali = new DateTime($row["tgl_kembali"]);
        $pinjam = $tgl_pinjam->format("d F Y");
        $kembali = $tgl_kembali->format("d F Y");
        echo "Peminjam : " . $row["nama"] . " dari tanggal " . $pinjam . " sampai tanggal " . $kembali . "<br>";
    }
} else {
    echo "0 result";
}
echo "<hr>";
$conn->close();
?>