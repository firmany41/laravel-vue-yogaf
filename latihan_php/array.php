<?php
$filename = 'data.json';
$data = file_get_contents($filename);
$users = json_decode($data);
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Daftar Nilai</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

</head>

<body>
    <div class="container mx-auto my-5">

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th scope="col">No.</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Tanggal Lahir</th>
                    <th scope="col">Umur</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Kelas</th>
                    <th scope="col">Nilai</th>
                    <th scope="col">Hasil</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $index = 1;
                foreach ($users as $user) {
                    $birthDate = new DateTime($user->tanggal_lahir);
                    $ttl = $birthDate->format("d F Y");
                    $today = new DateTime("today");
                    $umur = $today->diff($birthDate)->y;
                    $hasil = "";
                    if ($user->nilai >= 90) {
                        $hasil = "A";
                    } else if ($user->nilai >= 80 && $user->nilai < 90) {
                        $hasil = "B";
                    } else if ($user->nilai >= 70 && $user->nilai < 80) {
                        $hasil = "C";
                    } else {
                        $hasil = "D";
                    }
                ?>
                    <tr>
                        <td scope="row"> <?= $index; ?> </td>
                        <td> <?= $user->nama; ?> </td>
                        <td> <?= $ttl; ?> </td>
                        <td> <?= $umur; ?> Tahun</td>
                        <td> <?= $user->alamat; ?> </td>
                        <td> <?= $user->kelas; ?> </td>
                        <td> <?= $user->nilai; ?> </td>
                        <td> <?= $hasil; ?> </td>
                    </tr>

                <?php
                    $index++;
                }

                ?>
            </tbody>
        </table>
    </div>
</body>

</html>